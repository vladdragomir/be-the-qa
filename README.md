# Be the QA - 3PillarGlobal 

This repository contains the framework implementation from the Be the QA event. 

### Getting started

In order to be able to run the tests and use this framework, one just needs pull the code on the local machine and create a virtual environment for this specific project. 

Note: The `chromedriver` needs to be added into the `venv/bin` directory on MacOS or in the `venv/Scripts` on Windows. 

If there are issues with the chromedriver executable, one can add the location as an argument in the `browser_setup.py` file, where we create the driver: `driver = webdriver.Chrome("paths_to_chromedriver")`

