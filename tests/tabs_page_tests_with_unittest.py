import unittest
from pages.demo.demo_page import DemoPage
from pages.tabs.tabs_page import TabsLocators
from utils.browser_setup import setup_browser, close_browser


class TabsPageTests(unittest.TestCase):

    def setUp(self):
        self.driver = setup_browser()

    def tearDown(self):
        close_browser(self.driver)

    def test_paragraph_from_section_2(self):
        driver = self.driver
        expected_text = "Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In suscipit faucibus urna."
        demo_page = DemoPage(driver, navigate=True)
        tabs_page = demo_page.click_tabs()
        tabs_page.click_section(TabsLocators.SECTION_2)
        actual_text = tabs_page.get_paragraph_text(TabsLocators.SECTION_2_PARAGRAPH)
        assert actual_text == expected_text

    def test_all_tabs_are_visible(self):
        driver = self.driver
        demo_page = DemoPage(driver, navigate=True)
        demo_page.click_tabs()
        self.assertTrue(driver.find_element(*TabsLocators.FIRST_TAB).is_displayed())
        self.assertTrue(driver.find_element(*TabsLocators.SECOND_TAB).is_displayed())
        self.assertTrue(driver.find_element(*TabsLocators.THIRD_TAB).is_displayed())


if __name__ == "__main__":
    unittest.main()

