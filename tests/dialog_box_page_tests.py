from pages.demo.demo_page import DemoPage
from utils.browser_setup import setup_browser, close_browser


driver = setup_browser()
demo_page = DemoPage(driver, navigate=True)
dialog_box_page = demo_page.click_dialog_box()
dialog_box_page.create_new_user(name='admin', email='test@something', password='password')
assert dialog_box_page.check_row(2, name='admin', email='test@something', password='password'), 'Test has failed'
close_browser(driver)
