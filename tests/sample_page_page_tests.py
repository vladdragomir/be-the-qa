import time

from pages.sample_page.sample_page_page import SamplePagePage
from utils.browser_setup import setup_browser, close_browser


driver = setup_browser()
file_path = "/Users/vlad.dragomir/Personal/BeTheQA-session5/resources/upload.txt"
name = "Name"
email = "email@betheqa.com"
website = "http://www.google.com"
experience = "3-5"
expertise = "Automation Testing"
education = "Post Graduate"
comment = "Yay, it works!"

sample_page_page = SamplePagePage(driver, navigate=True)
sample_page_page.fill_out_form(file_path, name, email, website, experience, comment)
time.sleep(5)
sample_page_page.check_form_results(name, email, website, experience, expertise, education, comment)
close_browser(driver)

