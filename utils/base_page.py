from selenium.webdriver.chrome.webdriver import  WebDriver
from selenium.webdriver.support.select import Select

from utils.browser_setup import setup_browser


class BaseLocators:
    pass


class BasePage:
    PAGE = ""

    def __init__(self, driver: WebDriver, navigate=False):
        self.driver = driver
        if not driver:
            driver = setup_browser()
        if navigate:
            driver.get(self.PAGE)

    def switch_to_iframe(self, iframeLocator):
        iframe = self.driver.find_element(*iframeLocator)
        self.driver.switch_to.frame(iframe)

    def select_option_from_dropdown(self, locator, value):
        dropdown = Select(self.driver.find_element(*locator))
        dropdown.select_by_visible_text(value)