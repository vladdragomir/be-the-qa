from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from utils.base_page import BasePage


class TabsLocators:
    TITLE = (By.XPATH, "//div[@class='page_heading']/h1")
    TOGGLE_ICONS = (By.XPATH, "//li[@id='Toggle Icons']")
    SECTION_IFRAME = (By.XPATH, "//iframe[@src='../../demoSite/practice/accordion/collapsible.html']")
    SECTION_2 = (By.XPATH, "//h3[text()='Section 2']")
    SECTION_2_PARAGRAPH = (By.XPATH, "//div[@id='ui-id-4']/p")
    FIRST_TAB = (By.XPATH, "//li[text()='Simple Accordion']")
    SECOND_TAB = (By.XPATH, "//li[text()='Re-Size Accordion']")
    THIRD_TAB = (By.XPATH, "//li[text()='Toggle Icons']")


class TabsPage(BasePage):
    PAGE = "http://www.globalsqa.com/demo-site/accordion-and-tabs/"

    def __init__(self, driver, navigate):
        super().__init__(driver, navigate)
        WebDriverWait(driver, 5).until(EC.element_to_be_clickable(TabsLocators.TOGGLE_ICONS))

    def click_section(self, section):
        self.switch_to_iframe(TabsLocators.SECTION_IFRAME)
        self.driver.find_element(*section).click()

    def get_paragraph_text(self, paragraph):
        WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located(paragraph))
        paragraph_element = self.driver.find_element(*paragraph)
        return paragraph_element.text
