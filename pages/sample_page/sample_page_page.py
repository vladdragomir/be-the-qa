from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from utils.base_page import BasePage


class SamplePageLocators:
    TITLE = (By.XPATH, "//div[@class='page_heading']/h1")
    FILE_UPLOAD = (By.XPATH, '//input[@type="file"]')
    EXPERIENCE_DROPDOWN = (By.XPATH, "//select[@id='g2599-experienceinyears']")
    NAME = (By.XPATH, "//input[@id='g2599-name']")
    EMAIL = (By.XPATH, "//input[@id='g2599-email']")
    WEBSITE = (By.XPATH, "//input[@id='g2599-website']")
    EXPERTISE = (By.XPATH, "//input[@value='Automation Testing']")
    EDUCATION = (By.XPATH, "//input[@value='Post Graduate']")
    COMMENT = (By.XPATH, "//textarea[@id='contact-form-comment-g2599-comment']")
    SUBMIT_BTN = (By.XPATH, "//p[@class='contact-submit']/input")
    NAME_TEMPLATE = (By.XPATH, "//p[text()='Name: {}']")
    EMAIL_TEMPLATE = (By.XPATH, "//p[text()='Email: {}']")
    WEBSITE_TEMPLATE = (By.XPATH, "//p[text()='Website: {}']")
    EXPERIENCE_TEMPLATE = (By.XPATH, "//p[text()='Experience (In Years): {}']")
    EXPERTISE_TEMPLATE = (By.XPATH, "//p[text()='Expertise :: {}']")
    EDUCATION_TEMPLATE = (By.XPATH, "//p[text()='Education: {}']")
    COMMENT_TEMPLATE = (By.XPATH, "//p[text()='Comment: {}']")


class SamplePagePage(BasePage):
    PAGE = "http://www.globalsqa.com/samplepagetest/"

    def __init__(self, driver, navigate):
        super().__init__(driver, navigate)
        WebDriverWait(driver, 5).until(EC.visibility_of_element_located(SamplePageLocators.TITLE))

    def upload_file(self, file_path):
        element = self.driver.find_element(*SamplePageLocators.FILE_UPLOAD)
        element.send_keys(file_path)

    def select_experience(self, value):
        self.select_option_from_dropdown(SamplePageLocators.EXPERIENCE_DROPDOWN, value)

    def fill_out_form(self, file_path, name, email, website, experience, comment):
        SamplePagePage.upload_file(self, file_path=file_path)
        self.driver.find_element(*SamplePageLocators.NAME).send_keys(name)
        self.driver.find_element(*SamplePageLocators.EMAIL).send_keys(email)
        self.driver.find_element(*SamplePageLocators.WEBSITE).send_keys(website)
        SamplePagePage.select_experience(self, value=experience)
        self.driver.find_element(*SamplePageLocators.EXPERTISE).click()
        self.driver.find_element(*SamplePageLocators.EDUCATION).click()
        self.driver.find_element(*SamplePageLocators.COMMENT).send_keys(comment)
        self.driver.find_element(*SamplePageLocators.SUBMIT_BTN).click()

    def check_form_results(self, name, email, website, experience, expertise, education, comment):
        self.driver.find_element(SamplePageLocators.NAME_TEMPLATE[0], SamplePageLocators.NAME_TEMPLATE[1].format(str(name))).is_displayed()
        #self.driver.find_element(SamplePageLocators.EMAIL_TEMPLATE[0], SamplePageLocators.EMAIL_TEMPLATE[1].format(str(email))).is_displayed()
        self.driver.find_element(SamplePageLocators.WEBSITE_TEMPLATE[0], SamplePageLocators.WEBSITE_TEMPLATE[1].format(str(website))).is_displayed()
        self.driver.find_element(SamplePageLocators.EXPERIENCE_TEMPLATE[0], SamplePageLocators.EXPERIENCE_TEMPLATE[1].format(str(experience))).is_displayed()
        self.driver.find_element(SamplePageLocators.EXPERTISE_TEMPLATE[0], SamplePageLocators.EXPERTISE_TEMPLATE[1].format(str(expertise))).is_displayed()
        self.driver.find_element(SamplePageLocators.EDUCATION_TEMPLATE[0], SamplePageLocators.EDUCATION_TEMPLATE[1].format(str(education))).is_displayed()
        self.driver.find_element(SamplePageLocators.COMMENT_TEMPLATE[0], SamplePageLocators.COMMENT_TEMPLATE[1].format(str(comment))).is_displayed()


