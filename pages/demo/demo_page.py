from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.webdriver import WebDriver

from pages.dialog_box.dialog_box_page import DialogBoxPage
from pages.tabs.tabs_page import TabsPage
from utils.base_page import BasePage


class DemoLocators:
    PAGE_BUTTON = (By.XPATH, "//a[text()={}]")
    TABS = PAGE_BUTTON[0], PAGE_BUTTON[1].format('"Tabs"')
    DIALOG_BOX = PAGE_BUTTON[0], PAGE_BUTTON[1].format('"DialogBox"')
    DROP_DOWN = PAGE_BUTTON[0], PAGE_BUTTON[1].format('"DropDown"')
    PROGRESS_BAR = PAGE_BUTTON[0], PAGE_BUTTON[1].format('"ProgressBar"')


class DemoPage(BasePage):
    PAGE = "http://www.globalsqa.com/demo-site/"

    def __init__(self, driver: WebDriver, navigate: bool):
        super().__init__(driver, navigate)

    def click_tabs(self):
        tabs_button = self.driver.find_element(*DemoLocators.TABS)
        tabs_button.click()
        return TabsPage(self.driver, navigate=False)

    def click_dialog_box(self):
        dialog_box_button = self.driver.find_element(*DemoLocators.DIALOG_BOX)
        dialog_box_button.click()
        return DialogBoxPage(self.driver, navigate=False)
